use proto::buildstream::v2::reference_storage_server::ReferenceStorageServer;
use reference_proxy::ReferenceProxy;
use tonic::transport::Server;

pub mod proto;
pub mod reference_proxy;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let remote = std::env::args()
        .nth(1)
        .ok_or("Expected a remote address!")?;
    let port = std::env::args().nth(2).ok_or("Expected a port!")?;
    let addr = format!("[::1]:{}", port).parse()?;
    let proxy_server = ReferenceProxy::new(remote);

    Server::builder()
        .add_service(ReferenceStorageServer::new(proxy_server))
        .serve(addr)
        .await?;

    Ok(())
}
