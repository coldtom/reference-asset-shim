fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure().compile(
        &[
            "proto/build/bazel/remote/asset/v1/remote_asset.proto",
            "proto/build/bazel/remote/execution/v2/remote_execution.proto",
            "proto/build/bazel/semver/semver.proto",
            "proto/google/api/annotations.proto",
            "proto/google/api/http.proto",
            "proto/google/bytestream/bytestream.proto",
            "proto/google/longrunning/operations.proto",
            "proto/google/rpc/status.proto",
            "proto/buildstream/v2/buildstream.proto",
        ],
        &["proto"],
    )?;
    //  tonic_build::compile_protos("proto/buildstream/v2/buildstream.proto")?;
    //  tonic_build::compile_protos("proto/build/bazel/remote/asset/v1/remote_asset.proto")?;
    Ok(())
}
