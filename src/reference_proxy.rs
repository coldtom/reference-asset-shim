use tonic::{Code, Request, Response, Status};

use crate::proto::buildstream::v2::reference_storage_server::ReferenceStorage;
use crate::proto::buildstream::v2::{GetReferenceRequest, GetReferenceResponse};
use crate::proto::buildstream::v2::{StatusRequest, StatusResponse};
use crate::proto::buildstream::v2::{UpdateReferenceRequest, UpdateReferenceResponse};

use crate::proto::build::bazel::remote::asset::v1::fetch_client::FetchClient;
use crate::proto::build::bazel::remote::asset::v1::push_client::PushClient;
use crate::proto::build::bazel::remote::asset::v1::FetchBlobRequest;
use crate::proto::build::bazel::remote::asset::v1::PushBlobRequest;

#[derive(Debug)]
pub struct ReferenceProxy {
    remote_address: String,
}

impl ReferenceProxy {
    pub fn new(remote_address: String) -> Self {
        Self { remote_address }
    }
}

#[tonic::async_trait]
impl ReferenceStorage for ReferenceProxy {
    async fn get_reference(
        &self,
        request: Request<GetReferenceRequest>,
    ) -> Result<Response<GetReferenceResponse>, Status> {
        println!("GetReference: {:?}", request);
        let request = request.into_inner();

        let mut fetch_client = match FetchClient::connect(self.remote_address.clone()).await {
            Ok(client) => client,
            Err(_) => {
                return Err(Status::new(
                    Code::Unavailable,
                    "Remote Service is unavailable",
                ))
            }
        };

        let fetch_request = FetchBlobRequest {
            instance_name: request.instance_name,
            timeout: None,
            oldest_content_accepted: None,
            uris: vec![format!("urn:fdc:buildstream.build:v1:{}", request.key)],
            qualifiers: Vec::new(),
        };

        let fetch_response = fetch_client.fetch_blob(fetch_request).await?;
        let fetch_response = fetch_response.into_inner();

        let response = GetReferenceResponse {
            digest: fetch_response.blob_digest,
        };

        Ok(Response::new(response))
    }

    async fn update_reference(
        &self,
        request: Request<UpdateReferenceRequest>,
    ) -> Result<Response<UpdateReferenceResponse>, Status> {
        println!("UpdateReference: {:?}", request);
        let request = request.into_inner();

        let mut push_client = match PushClient::connect(self.remote_address.clone()).await {
            Ok(client) => client,
            Err(_) => {
                return Err(Status::new(
                    Code::Unavailable,
                    "Remote Service is unavailable",
                ))
            }
        };

        let push_request = PushBlobRequest {
            instance_name: request.instance_name,
            uris: request
                .keys
                .iter()
                .map(|s| format!("urn:fdc:buildstream.build:2020:v1:{}", s))
                .collect(),
            qualifiers: Vec::new(),
            expire_at: None,
            blob_digest: request.digest,
            references_blobs: Vec::new(),
            references_directories: Vec::new(),
        };

        push_client.push_blob(push_request).await?;

        let response = UpdateReferenceResponse {};

        Ok(Response::new(response))
    }

    async fn status(
        &self,
        request: Request<StatusRequest>,
    ) -> Result<Response<StatusResponse>, Status> {
        println!("Status: {:?}", request);
        // Naïvely assume that the remote server allows updates
        // We should do a PushBlob request to make sure.
        let response = StatusResponse {
            allow_updates: true,
        };

        Ok(Response::new(response))
    }
}
