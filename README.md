# BuildStream Reference-Asset Shim

A lightweight gRPC proxy, converting [BuildStream] `Reference` requests into
[Remote Asset API] requests, written in Rust.

BuildStream 2 will replace the `Reference` service used for the 1.x remote
artifact server with a more general approach for better integration with
the Remote Execution API. This means that in order to support BuildStream
1.x and master you need to run two services - a `bst-artifact-server` and
a ContentAddressableStorage from a REAPI server. This replaces the
`bst-artifact-server` to use the Remote Asset API implementation needed
for BuildStream 2, which should allow for more reliable storage and
better integration.

[BuildStream]: https://buildstream.build
[Remote Asset API]: https://github.com/bazelbuild/remote-apis